﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T item)
        {
            Data.Add(item);
            return Task.CompletedTask;
        }

        public Task<bool> DeleteAsync(T item)
        {
            return Task.FromResult(Data.Remove(item));
        }

        public Task<bool> UpdateAsync(T item)
        {
            var updatedItem = Data.FirstOrDefault(x => x.Id == item.Id);
            if (updatedItem == null)
                return Task.FromResult(false);

            updatedItem = item;

            return Task.FromResult(true);
        }
    }
}